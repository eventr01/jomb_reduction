from __future__ import division
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib import gridspec
import numpy as np
from simulation_functions import simulate_full
from AMS_functions import update_timeandtau, update_vect
from basic_functions import kon, koff, my_random_choice
from parameters_toggleswitch import G, theta, K0, K1, D1, B, M, S, sigma, S0, S1, D0

### Figure 3

# PARAMETERS FOR SIMULATION
t_max = 1000
nb_step = 2 * t_max
t_simul = np.linspace(0, t_max, nb_step)

# INITIAL CONDITIONS
P_init = 0.05
P0 = P_init * np.ones(G)
E0 = np.zeros(G)

eq_st = list()

eps = 7
cell_E1, cell_P1 = simulate_full(E0, P0, t_simul, theta, K0 * eps, K1 * eps, D1, B * eps, M, S, sigma, G, kon, koff, my_random_choice,
                  update_timeandtau, update_vect)

eps = 30
cell_E2, cell_P2 = simulate_full(E0, P0, t_simul, theta, K0 * eps, K1 * eps, D1, B * eps, M, S, sigma, G, kon, koff, my_random_choice,
                  update_timeandtau, update_vect)

fig = plt.figure(figsize=(15, 5))
spec = gridspec.GridSpec(ncols=2, nrows=2,
                         height_ratios=[1, 6])

ax0 = fig.add_subplot(spec[0])
ax0.yaxis.set_major_locator(MaxNLocator(integer=True))
ax1 = fig.add_subplot(spec[1])
ax2 = fig.add_subplot(spec[2])
ax3 = fig.add_subplot(spec[3])
line1, = ax0.plot(t_simul, cell_E1[:, 0], linewidth=1, color='blue', alpha=1)
line2, = ax0.plot(t_simul, cell_E1[:, 1], linewidth=1, color='orange', alpha=1)
line3, = ax2.plot(t_simul, cell_P1[:, 0], linewidth=1, color='blue', alpha=1)
line4, = ax2.plot(t_simul, cell_P1[:, 1], linewidth=1, color='orange', alpha=1)
line5, = ax1.plot(t_simul, cell_E2[:, 0], linewidth=1, color='blue', alpha=1)
line6, = ax1.plot(t_simul, cell_E2[:, 1], linewidth=1, color='orange', alpha=1)
line7, = ax3.plot(t_simul, cell_P2[:, 0], linewidth=1, color='blue', alpha=1)
line8, = ax3.plot(t_simul, cell_P2[:, 1], linewidth=1, color='orange', alpha=1)
ax2.set_xlabel('time')
ax3.set_xlabel('time')
ax2.set_ylim(0, 0.4)
ax3.set_ylim(0, 0.4)
ax2.set_ylabel('gene expression (protein concentration)')
ax0.set_ylabel('promoter state')
ax2.legend([line3, line4], ['gene1', 'gene 2'])
plt.show()


