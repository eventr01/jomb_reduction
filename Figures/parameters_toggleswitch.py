"""Network parameters"""
"""The model for the function kon and the meaning of these parameters are described in Appendices A&E&F"""
import numpy as np

### Example of 2 genes toggle-switch, with 3 basins
theta = np.array([[7, -7], [-7, 7]])

# Example of a 3 genes toggle-switch, with 4 basins
# theta = np.array([[6.2, -5, -5], [-4, 5, -7], [-5, -4, 4]])

### Example of 4 genes network with negative feedback loop, with 4 basins
# G = 4
# theta = np.zeros((G, G))
# theta[1, 0], theta[2, 0], theta[3, 2] = 10, 10, 12
# theta[0, 3] = -10
# theta[0, 0] = 7

### Global settings
G = np.size(theta, 0) # Number of genes
S0 = 1*np.ones(G) # mRNA creation rates
D0 = 1*np.ones(G) # mRNA degradation rates
S1 = 1*np.ones(G) # Protein creation rates
D1 = 0.2*np.ones(G) # Protein degradation rates

### Specific model parameters
K0 = (1/80) * np.ones(G) # basal promoter frequency
K1 = (31/80) * np.ones(G) # highest promoter frequency
B = (10/8) * np.ones(G) # burst size
M = 3. * np.ones((G,G)) # order of the interaction between Transcription factor and promoter
sigma = np.outer(np.ones(G), S0*S1/(D0*D1)) # ratio coming from the full model described in Appendix A

# Thresold value
S = sigma.copy()
for i in range(0,G):
    for j in range(0,G):
        if i != j:
            S[j,i] = sigma[j,i] * np.exp(-theta[i,i]/M[i,i])
