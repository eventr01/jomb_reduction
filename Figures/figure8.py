from __future__ import division
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
from scipy.special import beta

from functions_main import main_algo_LD
from AMS_functions import score_function, bernouilli_func, update_timeandtau, update_vect, check_attractor
from simulation_functions import simulate_until_crossing, simulate_for_convergence
from basic_functions import kon, koff, my_random_choice, log_stationary_distribution
from parameters_toggleswitch import G, theta, K0, K1, D1, B, M, S, sigma, S0, S1, D0

from numba.errors import NumbaDeprecationWarning, NumbaPendingDeprecationWarning
import warnings
warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPendingDeprecationWarning)

### Figure 8

### SIMULATION PARAMETERS

eps = 7

err = 0.001
tmax_simul = int(10/(np.min(D1)))
nb_step_simul = int(np.sum(B) * tmax_simul)
nb_simul = 200
nb_simul_MC = 3000

t_simul = np.linspace(0, tmax_simul, nb_step_simul)
tmax = 10 * t_simul[-1]
nb_step = int(np.sum(B) * tmax)
t = np.linspace(0, tmax, nb_step)
max_iter = 100

### AMS PARAMETERS
n_rep_AMS = 10
seuil_basin = 0

### Specific parameters
tot = 50 # the mean will be computed on 50 trajectories of exit
Z_from = 1 # Initial basin
Z_to = 2 # Final basin

"""This function allows find the points of a fluctuation paths located a distance greater than a certain thresold"""
def find_init(seuil, backward):
    P0 = backward[-1]
    liste = []
    i = 1
    while np.sum(np.abs(backward[-i] - P0)**2) <= seuil:
        liste.append(backward[-i])
        i += 1
    liste.append(backward[-i])
    return liste


### Find equilibrium points, number of basins, saddle points, exit trajectories and exit costs

nb_Z, eq_st, mat_cout_LD, mat_id_traj_opt, mat_liste_traj_opt, eq_inst, eq_inst_liste = main_algo_LD(
                                                                G, theta, K0, K1, D1, B, M, S, sigma, err,
                                                                nb_simul, t_simul, tmax, nb_step, t, max_iter)

backward = eq_inst_liste[Z_from][2][mat_id_traj_opt[Z_from, Z_to]] # optimal trajectory between basin Z_from and basin Z_to
x_inst = eq_inst_liste[Z_from][0][mat_id_traj_opt[Z_from, Z_to]] # coordinate of the saddle point between basin Z_from and basin Z_to

K0_eps = K0 * eps
K1_eps = K1 * eps
B_eps = B * eps
kon_eq = np.array([kon(eq_st[z], theta, K0_eps, K1_eps, S, sigma, M, G) for z in range(0, nb_Z)])
bet_eq = np.ones(nb_Z)
for z in range(0, nb_Z):
    for i in range(0, G):
        bet_eq[z] = bet_eq[z] * beta(kon_eq[z][i] / D1[i], koff(eq_st[z], B_eps)[i] / D1[i])
mu_eq = np.ones(nb_Z)

# BUILD THE MATRIX OF THE SCORE FUNCTION ON EVERY SADDLE POINTS

mat_score_saddle_point = np.zeros((nb_Z, nb_Z))
for k in range(0, nb_Z):
    for l in range(0, nb_Z):
        if mat_id_traj_opt[k, l] > -1:
            x_inst = eq_inst_liste[k][0][mat_id_traj_opt[k, l]]
            mat_score_saddle_point[k, l] = score_function(
                x_inst, k, l, eq_st, bet_eq, mu_eq, kon_eq, koff(x_inst, B_eps), D1)

### Core of the figure

# Draw the image on the backside : the equilibrium points, the saddle point, the beta approximation within each basin
dx = 100
lim = 0.35
x = np.linspace(0.001, lim, int(dx * lim))
y = np.linspace(0.001, lim, int(dx * lim))
cmap = plt.get_cmap('Greys')
xtmp_st = [eq_st[k][0] for k in [Z_from, Z_to]]
ytmp_st = [eq_st[k][1] for k in [Z_from, Z_to]]
xtmp_inst = [eq_inst[4][0]]
ytmp_inst = [eq_inst[4][1]]
bet = np.array(
    [[log_stationary_distribution(np.array([i, j]), mu_eq, kon_eq, koff(np.array([i, j]), B_eps), D1) for i
      in x] for j in y])

min_bet = int(np.min(bet)) - 1
max_bet = int(np.max(bet)) + 1
levels = MaxNLocator(nbins=100).tick_values(-10, max_bet)

# Plot the relaxation and fluctuation trajectories between the two saddle points

fig, ax = plt.subplots(figsize=(5, 5))
ax.set_xlabel('gene 1')
ax.set_ylabel('gene 2')
ax.set_xlim(0,lim)
ax.set_ylim(0,lim)
cf1 = ax.contourf(x[:], y[:], bet, levels=levels, cmap=cmap, alpha=.4)
ax.scatter(xtmp_st + xtmp_inst, ytmp_st + ytmp_inst, c='black', s=20)

if len(mat_liste_traj_opt[Z_from][Z_to]) > 0: # if there exists optimal trajectories between the two basins
    for h in mat_liste_traj_opt[Z_from][Z_to]: # find all the optimal trajectories (i.e linking a saddle point and the equilibrium)
        sol_rev = eq_inst_liste[Z_from][Z_to][h].copy()
        sol_det = np.zeros((1, 2))
        for num_inst, x_inst in enumerate(eq_inst_liste[Z_to][3]):
            if np.sum(np.abs(x_inst - eq_inst_liste[Z_from][0][h])) < err:
                sol_det = eq_inst_liste[Z_to][4][num_inst].copy()
        # Plot the optimal trajectories
        if h == mat_id_traj_opt[Z_from, Z_to]:
            ax.plot(sol_rev[:, 0], sol_rev[:, 1], linewidth=1, color='red', alpha=1)
            ax.plot(sol_det[:, 0], sol_det[:, 1], linewidth=1, color='blue', alpha=1)


score_obj = score_function(eq_st[Z_to], Z_from, Z_to, eq_st, bet_eq, mu_eq, kon_eq,
                                   koff(eq_st[Z_to], B_eps), D1)
sim_P0 = find_init(err, backward)
cnt = 0
ok = 0
nb_MC = 30

for z in range(0,nb_MC):

    print(z, cnt, ok)
    cnt = 0
    ok = 0 # is 1 when a trajectory has reached the basin Z_to
    i = 0 # counts the number of trajectories needed

    while ok != 1:

        sim_E = np.array([bernouilli_func(eq_st[1])])
        sim_P = np.array(sim_P0.copy())
        score_P = np.array(
            [score_function(sim_P[-1], Z_from, Z_to, eq_st, bet_eq, mu_eq, kon_eq, koff(eq_st[Z_from], B_eps), D1)])
        sim_T = np.array([0.])

        ok, sim_T, sim_E, sim_P, score_P = simulate_until_crossing(sim_T, sim_E, sim_P, score_P,
                                                                   Z_from, Z_to, nb_Z, eq_st, bet_eq, mu_eq, kon_eq,
                                                                   err, seuil_basin,
                                                                   mat_score_saddle_point,
                                                                   theta, K0_eps, K1_eps, D1, B_eps, M, S,
                                                                   sigma, G,
                                                                   kon, koff, my_random_choice, score_function,
                                                                   update_timeandtau, update_vect,
                                                                   check_attractor)

        if ok == 1:
            x_moy_MC_lenmax_back, y_moy_MC_lenmax_back = sim_P[:, 0], sim_P[:, 1]
            if z < 1:
                ax.plot(x_moy_MC_lenmax_back, y_moy_MC_lenmax_back, linewidth=1, color='red', alpha=1)
            else:
                ax.plot(x_moy_MC_lenmax_back, y_moy_MC_lenmax_back, linewidth=0.3, color='black', alpha=0.5)

        cnt += 1

    ok = 0

    sim_P0_tmp = sim_P.copy()
    E0_tmp = sim_E[-1].copy()
    score_P0_tmp = score_P.copy()
    tmp = len(sim_P)

    while ok != 1:

        ## temps sortie et temps retour moyens
        score_P = score_P0_tmp.copy()
        sim_P = sim_P0_tmp.copy()
        ok, T, sim_P, score_P = simulate_for_convergence(E0_tmp, sim_P, score_P, score_obj, 1, 2, nb_Z,
                                                         eq_st, bet_eq, mu_eq, kon_eq,
                                                         theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G, kon,
                                                         koff, my_random_choice, score_function,
                                                         update_timeandtau, update_vect)

        if ok == 1:
            x_moy_MC_lenmax_for, y_moy_MC_lenmax_for = sim_P[tmp:, 0], sim_P[tmp:, 1]
            if z < 1:
                ax.plot(x_moy_MC_lenmax_for, y_moy_MC_lenmax_for, linewidth=1, color='blue', alpha=1)
            else:
                ax.plot(x_moy_MC_lenmax_for, y_moy_MC_lenmax_for, linewidth=0.3, color='black', alpha=0.5)

        cnt += 1

plt.show()




