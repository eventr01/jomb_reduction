from __future__ import division
import matplotlib.pyplot as plt
import math as mt
from sympy import Symbol, derive_by_array, diff
import numpy as np
from scipy.special import beta

from filtre_functions import my_filtre, my_order_comp, my_filtre_ratio, my_filtre_similar_2, \
                            my_filtre_similar_3, my_filtre_comp
from MFPT_functions import build_eqst, build_eqinst, build_from_eqinst_to_eqst, build_togd, \
                            my_gradient_descent, find_eqinst
from lagrangian_functions import Lag_opt_for_deriv
from AMS_functions import main_AMS, score_function, find_position_first_exit, \
                            bernouilli_func, find_seuil_attracteur, update_timeandtau, \
                            update_vect, check_attractor
from simulation_functions import simulate_until_crossing, simulate_for_MFPT
from basic_functions import kon, koff, my_random_choice, build_matetat


def main_algo_LD(G, theta, K0, K1, D1, B, M, S, sigma,
                 err, nb_simul, t_simul, tmax, nb_step, t, max_iter):
    """This function allows to recover the main parameters from the Large deviations analysis"""

    # PARAMETRES INVARIANTS
    err_forte = err / 1000
    subdiv = 1 / err

    # Initial conditions
    P_init = 5e-2
    P0 = P_init * np.ones(G)
    E0 = bernouilli_func(kon(P0, theta, K0, K1, S, sigma, M, G) / (kon(P0, theta, K0, K1, S, sigma, M, G)) + koff(P0, B))

    # Find equilibrium points

    eq_st = list()

    for _ in range(0, nb_simul):
        sim, cell = build_eqst(E0, P0, t_simul, t, theta, D1, K0, K1, S, sigma, M, B, G)
        eq_st.append(sim[-1, :])
    eq_st, mu_eq = my_filtre_ratio(eq_st, err)
    eq_st, mu_eq = my_order_comp(eq_st, mu_eq)
    nb_Z = len(eq_st)

    print('number of equilibrium points:', len(eq_st), eq_st)

    # Find the saddle points

    pts_to_gd = my_filtre(build_togd(eq_st, eq_st, subdiv, err, theta, D1, K0, K1, S, sigma,M, B, G), err)

    eq_inst = list()
    X_sy = [Symbol('x{}'.format(i)) for i in range(0, G)]
    X_ar = np.array(X_sy)
    lag = Lag_opt_for_deriv(X_ar, theta, D1, K0, K1, S, sigma, M, B, G)
    jac_lag = derive_by_array(lag, X_sy)
    hess_lag = derive_by_array(jac_lag, X_sy)

    for i in range(0, len(pts_to_gd)):
        r = my_gradient_descent(err_forte, max_iter, X_sy, jac_lag, hess_lag, pts_to_gd[i], theta, D1, K0, K1, S, sigma, M, B, G)
        if r[0] != 0:
            eq_inst.append(r)

    eq_inst = my_filtre(eq_inst, err)
    eq_inst = my_filtre_comp(eq_inst, eq_st, err)

    # Find others

    pts_to_gd = my_filtre(build_togd(eq_inst, eq_st, subdiv, err, theta, D1, K0, K1, S, sigma, M, B, G), err)

    for i in range(0, len(pts_to_gd)):
        r = my_gradient_descent(err_forte, max_iter, X_sy, jac_lag, hess_lag, pts_to_gd[i], theta, D1, K0, K1, S, sigma, M, B, G)
        if r[0] != 0: # if there is convergence of the gradient descent algorithm
            eq_inst.append(r)

    eq_inst = my_filtre(eq_inst, err)
    eq_inst = my_filtre_comp(eq_inst, eq_st, err)

    print('number of saddle points', len(eq_inst))

    # List of saddle point associated to every equilibrium points

    mat_etat = [build_matetat(i) for i in range(1,G+1)]

    eq_inst_liste = list()
    eq_inst_v1 = eq_inst.copy()

    for _ in range(0, nb_Z):
        eq_inst_liste.append([[], [], [], [], []])

    ### 0 - list of saddle points associated to an attractor by a fluctuation trajectory
    ### 1 - cost of the trajectory associated to each saddle point
    ### 2 - fluctuation trajectory
    ### 3 - saddle point associated to an attractor by a relaxation trajectory
    ### 4 - associated relaxation trajecory

    for k in range(0, len(eq_inst_v1)):
        r = build_eqinst(eq_inst_v1[k], eq_inst_liste, eq_st, mat_etat, err, find_eqinst, t, nb_step, tmax, theta, D1, K0, K1, S, sigma, M, B, G)
        for i in range(0, nb_Z):
            for j in range(0,len(r[i][0])):
                eq_inst_liste[i][0].append(r[i][0][j])
                eq_inst_liste[i][1].append(r[i][1][j])
                eq_inst_liste[i][2].append(r[i][2][j])
            for j in range(0, len(r[i][3])):
                eq_inst_liste[i][3].append(r[i][3][j])
                eq_inst_liste[i][4].append(r[i][4][j])

    for k in range(0, nb_Z):
        if eq_inst_liste[k][0]:
            eq_inst_liste[k][0], eq_inst_liste[k][1], eq_inst_liste[k][2] = my_filtre_similar_3(eq_inst_liste[k][0], eq_inst_liste[k][1],
                                                                                     eq_inst_liste[k][2], err)
            eq_inst_liste[k][3], eq_inst_liste[k][4] = my_filtre_similar_2(eq_inst_liste[k][3], eq_inst_liste[k][4], err)

    # Build the transition matrix

    mat_cout_LD = float('inf') * np.ones((nb_Z, nb_Z)) - np.diag(np.ones(nb_Z))

    mat_id_traj_opt = - np.ones((nb_Z, nb_Z), dtype=int)
    mat_liste_traj_opt = [[[] for _ in range(0, nb_Z)] for _ in range(0, nb_Z)]

    for k, x_init in enumerate(eq_st):
        for l, x_fin in enumerate(eq_st):
            if k != l:
                id_min, points_accessibles = build_from_eqinst_to_eqst(eq_inst_liste, k, l, err)
                mat_id_traj_opt[k, l] = id_min
                mat_liste_traj_opt[k][l] = points_accessibles.copy()
                ### id_min > 1 if there is a saddle point which is linked on both sides by a relaxation and a fluctuation trajectory
                if id_min > -1:
                    mat_cout_LD[k, l] = eq_inst_liste[k][1][id_min]

    return nb_Z, eq_st, mat_cout_LD, mat_id_traj_opt, mat_liste_traj_opt, eq_inst, eq_inst_liste


def actualize_metastable_parameters(eps, nb_Z, eq_st, nb_simul, t, err, theta, D1, K0, K1, S, sigma, M, B, G):
    """This function allows to actualize the parameters from the Large deviations function "main_algo_LD", which depend on epsilon"""

    # CONDITIONS INITIALES
    tmax_simul = int(10 / np.min(D1))
    nb_step_simul = int(np.sum(B) * tmax_simul)
    t_simul = np.linspace(0, tmax_simul, nb_step_simul)
    # P0 = np.random.uniform(0, 1, G)
    P0 = 5e-2 * np.ones(G)
    E0 = bernouilli_func(kon(P0, theta, K0, K1, S, sigma, M, G))

    # TRACER POINTS EQ

    eq_st_tmp = list()

    for _ in range(0, nb_simul):
        sim, cell = build_eqst(E0, P0, t_simul, t, theta, D1, K0, K1, S, sigma, M, B, G)
        eq_st_tmp.append(sim[-1, :])

    eq_st_tmp, mu_eq = my_filtre_ratio(eq_st_tmp, err)
    eq_st_tmp, mu_eq = my_order_comp(eq_st_tmp, mu_eq)

    kon_eq = np.array([kon(eq_st[z], theta, K0, K1, S, sigma, M, G) for z in range(0, nb_Z)])
    bet_eq = np.ones(nb_Z)
    for z in range(0, nb_Z):
        bet_eq[z] = np.prod(beta(kon_eq[z] / D1, koff(eq_st[z], B) / D1))

    return mu_eq, kon_eq, bet_eq



def function_MFPT(Z_from, Z_to, eps, nb_Z, nb_simul_MC, eq_st, bet_eq, mu_eq, kon_eq, koff, seuil_basin,
              mat_score_saddle_point, theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G):
    """This function provides the MFPT from the basin Z_from to the basin Z_to, obtained by a crude MC method"""

    # Identification of the scores to reach other basins
    P0_MFPT = np.array([eq_st[Z_from]])
    score0_MFPT = np.array([np.zeros(nb_Z)])
    score_obj_MFPT = np.zeros(nb_Z)
    for Z in range(0, nb_Z):
        score0_MFPT[0][Z] = score_function(eq_st[Z_from], Z_from, Z, eq_st, bet_eq, mu_eq, kon_eq, koff(eq_st[Z], B_eps), D1)
        score_obj_MFPT[Z] = score_function(eq_st[Z], Z_from, Z, eq_st, bet_eq, mu_eq, kon_eq, koff(eq_st[Z], B_eps), D1)

    # Computation of the MFPT
    MFPT_MC = list()
    MFPT_count = np.zeros(nb_Z, dtype=int)
    cnt = 0
    while np.sum(MFPT_count > nb_simul_MC / nb_Z) < nb_Z - 1: # Stop when there is enough trajectories of exit
        sim_P = P0_MFPT.copy()
        score_P = score0_MFPT.copy()
        E0 = bernouilli_func(eq_st[Z_from])
        num_basin, T, sim_P, score_P = simulate_for_MFPT(E0, sim_P, score_P, score_obj_MFPT, Z_from, nb_Z, eq_st, bet_eq,
                                                         mu_eq, kon_eq, seuil_basin, mat_score_saddle_point,
                                                         theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G,
                                                         kon, koff, my_random_choice, score_function,
                                                         update_timeandtau, update_vect)
        MFPT_MC.append(T)
        MFPT_count[num_basin] += 1
        cnt += 1

    return np.mean(MFPT_MC) * cnt / MFPT_count[Z_to]


def function_AMS(Z_from, Z_to, nb_Z, nb_simul_AMS, n_rep_AMS, eq_st, bet_eq, mu_eq, kon_eq,
             seuil_attracteur, seuil_basin, mat_score_saddle_point,
             theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G):
    """This function provides the probability of exit between the basins Z_from and Z_to with the AMS method"""

    proba_AMS = list()

    cnt = 0
    while cnt < nb_simul_AMS:

        ### Conditions initiales de sorties
        E0 = bernouilli_func(eq_st[Z_from])
        sim_E = np.array([E0])
        sim_P = np.array([eq_st[Z_from]])
        score_P = np.array(
            [score_function(eq_st[Z_from], Z_from, Z_to, eq_st, bet_eq, mu_eq, kon_eq, koff(eq_st[Z_from], B_eps), D1)])
        T, sim_E, sim_P, score_P = find_position_first_exit(sim_E, sim_P, score_P, Z_from, Z_to, nb_Z, eq_st, bet_eq,
                                                            mu_eq, kon_eq,
                                                            seuil_attracteur, seuil_basin,
                                                            mat_score_saddle_point,
                                                            theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G,
                                                            kon, koff, my_random_choice,
                                                            score_function, update_timeandtau,
                                                            update_vect)

        ### proba
        sim_T = np.array([0.])
        proba_tmp, traj_T_AMS, traj_E_AMS, traj_P_AMS, score_traj_P_AMS = \
            main_AMS(sim_T, sim_E, sim_P, score_P, Z_from, Z_to, nb_Z, eq_st, bet_eq, mu_eq,
                     kon_eq, mat_score_saddle_point, seuil_attracteur, seuil_basin,
                     n_rep_AMS, theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G,
                     kon, koff, my_random_choice, score_function,
                     update_timeandtau, update_vect, check_attractor)
        if proba_tmp > -1:
            proba_AMS.append(proba_tmp)
            cnt += 1

    print('proba AMS = ', np.mean(proba_AMS))

    return np.mean(proba_AMS)


def function_MC(Z_from, Z_to, nb_Z, compute_MC, nb_simul_MC, eq_st, eq_inst_liste, bet_eq, mu_eq, kon_eq,
             seuil_attracteur, seuil_basin, mat_score_saddle_point, mat_id_traj_opt,
                theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G):
    """This function provides the probability of exit between the basins Z_from and Z_to with the MC method"""

    T_sortie_MC = list()
    T_retour_MC = list()
    T_sortie_bassin_MC = list()

    nb_return_MC = 0
    nb_ok_MC = nb_simul_MC
    if compute_MC: # If it is wanted to return an accurate MC probaiblity
        nb_ok_MC = 0
    cnt = 0
    while cnt < nb_simul_MC or nb_ok_MC < nb_simul_MC or nb_return_MC < 1:

        ## Exit time and mean return time $T_{Z_from,Z_from}$

        E0 = bernouilli_func(eq_st[Z_from])
        sim_E = np.array([E0])
        sim_P = np.array([eq_st[Z_from]])
        score_P = np.array(
            [score_function(eq_st[Z_from], Z_from, Z_to, eq_st, bet_eq, mu_eq, kon_eq, koff(eq_st[Z_from], B_eps), D1)])
        T, sim_E, sim_P, score_P = find_position_first_exit(sim_E, sim_P, score_P, Z_from, Z_to, nb_Z, eq_st,
                                                            bet_eq, mu_eq, kon_eq,
                                                            seuil_attracteur, seuil_basin,
                                                            mat_score_saddle_point,
                                                            theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma,
                                                            G,
                                                            kon, koff, my_random_choice,
                                                            score_function, update_timeandtau,
                                                            update_vect)
        T_sortie_MC.append(T)

        sim_T = np.array([0.])
        ok, sim_T, sim_E, sim_P, score_P = simulate_until_crossing(sim_T, sim_E, sim_P, score_P,
                                                                   Z_from, Z_to, nb_Z, eq_st, bet_eq, mu_eq, kon_eq,
                                                                   seuil_attracteur, seuil_basin,
                                                                   mat_score_saddle_point,
                                                                   theta, K0_eps, K1_eps, D1, B_eps, M, S,
                                                                   sigma, G,
                                                                   kon, koff, my_random_choice, score_function,
                                                                   update_timeandtau, update_vect,
                                                                   check_attractor)

        if ok == -1:
            T_retour_MC.append(sim_T[-1])
            nb_return_MC += 1

        elif ok == 1:
            nb_ok_MC += 1
            T_sortie_bassin_MC.append(sim_T[-1])

        cnt += 1

    temps_sortie_MC = np.mean(T_sortie_MC)
    proba_MC = nb_ok_MC / cnt
    temps_sortie_bassin_MC = np.mean(T_sortie_bassin_MC)
    temps_retour_MC = np.mean(T_retour_MC)

    print('proba MC = ', proba_MC)

    return proba_MC, temps_sortie_MC, temps_retour_MC, temps_sortie_bassin_MC


def main_algo_numerical(Z_from, Z_to, eps, eps_max_MC, theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G,
                  n_rep_AMS, nb_simul_MC, nb_simul_AMS, seuil_basin,
                  nb_Z, eq_st, nb_simul, mat_id_traj_opt, eq_inst_liste,
                  mu_eq, kon_eq, bet_eq, mat_score_saddle_point):
    """This function provides all the quantities given by the numerical methods (probabilities and MFPTs)"""

    ### seuil
    seuil_attracteur = find_seuil_attracteur(eq_st[Z_from], nb_simul, theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G,
                                             bernouilli_func, kon, koff)

    ### MFPT with MC method (if epsilon is bigger than 1 / 8)
    MFPT_MC = 1
    if eps < eps_max_MC:
        MFPT_MC = function_MFPT(Z_from, Z_to, eps, nb_Z, nb_simul_MC, eq_st, bet_eq, mu_eq, kon_eq, koff, seuil_basin,
                                  mat_score_saddle_point, theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G)

    ### MONTE CARLO for all quantities used in the Methods of the article (part 3)

    proba_MC, temps_sortie_MC, temps_retour_MC, \
    temps_sortie_bassin_MC = function_MC(Z_from, Z_to,
                                          nb_Z, eps < eps_max_MC, nb_simul_MC, eq_st, eq_inst_liste,
                                          bet_eq, mu_eq, kon_eq,
                                          seuil_attracteur, seuil_basin,
                                          mat_score_saddle_point, mat_id_traj_opt,
                                          theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma,
                                          G)

    ### AMS

    proba_AMS = function_AMS(Z_from, Z_to, nb_Z, nb_simul_AMS, n_rep_AMS, eq_st, bet_eq, mu_eq, kon_eq,
                                seuil_attracteur, seuil_basin, mat_score_saddle_point,
                                theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G)

    return proba_MC, proba_AMS, temps_retour_MC, temps_sortie_MC, \
            temps_sortie_bassin_MC, MFPT_MC


def display_proba_MFPT(Z_from, Z_to, eps, eps_max_MC, theta, K0, K1, D1, B, M, S, sigma, G, n_rep_AMS, nb_simul_MC, nb_simul_AMS,
                       seuil_basin, err, nb_Z, eq_st, nb_simul, t, mat_cout_LD, mat_id_traj_opt, eq_inst_liste):
    """This function provides all the quantities given by the numerical methods (probabilities and MFPTs) from main_algo_numerical
    And the same quantities given by the numerical methods (probabilities and MFPTs)."""

    ### parametres a modifier
    K0_eps = K0 * eps
    K1_eps = K1 * eps
    B_eps = B * eps

    # TROUVER LES PARAMS D'EQUILIBRES
    mu_eq, kon_eq, bet_eq = actualize_metastable_parameters(eps, nb_Z, eq_st, nb_simul, t, err, theta, D1, K0_eps, K1_eps, S, sigma, M, B_eps, G)

    ### initialisation des matrices
    proba_LD = np.exp(-mat_cout_LD * eps)[Z_from, Z_to]

    ## Build matrix of the score functions to reach between each basins
    mat_score_saddle_point = np.zeros((nb_Z, nb_Z))
    for k in range(0, nb_Z):
        for l in range(0, nb_Z):
            if mat_id_traj_opt[k, l] > -1:
                x_inst = eq_inst_liste[k][0][mat_id_traj_opt[k, l]]
                mat_score_saddle_point[k, l] = score_function(
                    x_inst, k, l, eq_st, bet_eq, mu_eq, kon_eq, koff(x_inst, B_eps), D1)

    ### Main action for obtaining matrices of probabilities and MFPTs

    proba_MC, proba_AMS, temps_retour_MC, temps_sortie_MC, \
    temps_sortie_bassin_MC, MFPT_MC = \
        main_algo_numerical(Z_from, Z_to, eps, eps_max_MC, theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G,
                  n_rep_AMS, nb_simul_MC, nb_simul_AMS, seuil_basin,
                  nb_Z, eq_st, nb_simul, mat_id_traj_opt, eq_inst_liste,
                  mu_eq, kon_eq, bet_eq, mat_score_saddle_point)

    return mu_eq, kon_eq, proba_LD, proba_MC, proba_AMS, temps_retour_MC, \
           temps_sortie_MC, temps_sortie_bassin_MC, MFPT_MC


def plot_figure(eps_max_MC, borne_eps_inf, borne_eps_sup, proba_LD, proba_AMS, proba_MC,
                temps_sortie_attracteur, temps_sortie_bassin, temps_retour_attracteur,
                temps_passage_MC):
    """This function allows to recover the figure 9 of the article, from the outputs of the functions main_algo_numerical and
    main_algo_LD for a serie of descreasing epsilon"""

    temps_passage_LD = list()
    temps_passage_AMS = list()

    x = np.array([eps for eps in range(borne_eps_inf, borne_eps_sup)])
    x_MC = np.array([eps for eps in range(borne_eps_inf, min(borne_eps_sup, eps_max_MC))])

    prefactor = np.log(np.mean(np.array(proba_LD[4:]) / np.array(proba_AMS[4:])))

    for eps in range(0, borne_eps_sup - borne_eps_inf):
        temps_passage_tmp_LD = temps_retour_attracteur[eps] * (1 / (proba_LD[eps] * np.exp(-prefactor))) + \
                                temps_sortie_attracteur[eps] * (1 / (proba_LD[eps] * np.exp(-prefactor))) + \
                                temps_sortie_bassin[eps]
        temps_passage_tmp_AMS = temps_retour_attracteur[eps] * (1 / proba_AMS[eps]) + \
                                temps_sortie_attracteur[eps] * (1 / (proba_AMS[eps])) + \
                                temps_sortie_bassin[eps]
        temps_passage_LD.append(temps_passage_tmp_LD)
        temps_passage_AMS.append(temps_passage_tmp_AMS)

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10, 5))
    axes[0].set_xlabel('$\\frac{1}{\epsilon}$')
    axes[0].set_ylabel('-ln(probability of exit)')
    axes[1].set_xlabel('$\\frac{1}{\epsilon}$')
    axes[1].set_ylabel('-ln(transition rates ($h^{-1}$))')

    # Plot the probabilities

    y_LD = np.array(
        [-mt.log(proba_LD[eps]) + prefactor for eps in range(0, borne_eps_sup - borne_eps_inf)])
    y_AMS = np.array(
        [-mt.log(proba_AMS[eps]) for eps in range(0, borne_eps_sup - borne_eps_inf)])
    y_MC = np.array(
        [-np.log(proba_MC[eps]) for eps in range(0, min(eps_max_MC, borne_eps_sup) - borne_eps_inf)])
    axes[0].plot(x, y_LD, color='red', marker='x', alpha=1)
    axes[0].plot(x_MC, y_MC, color='blue', marker='x', alpha=1)
    axes[0].plot(x, y_AMS, color='green', marker='x', alpha=1)

    # Plot the MFPTs
    y_MFPT_LD = np.array([temps_passage_LD[eps] for eps in range(0, borne_eps_sup - borne_eps_inf)])
    y_MFPT_AMS = np.array([temps_passage_AMS[eps] for eps in range(0, borne_eps_sup - borne_eps_inf)])
    y_MC = np.array([temps_passage_MC[eps] for eps in range(0, min(eps_max_MC, borne_eps_sup) - borne_eps_inf)])
    line1, = axes[1].plot(x, np.log(y_MFPT_LD), color='red', marker='x', alpha=1)
    line2, = axes[1].plot(x_MC, np.log(y_MC), color='blue', marker='x', alpha=1)
    line3, = axes[1].plot(x, np.log(y_MFPT_AMS), color='green', marker='x', alpha=1)

    axes[1].legend((line1, line2, line3), ('prediction LDP', 'prediction MC', 'prediction AMS'))

    fig.savefig('results', bbox_inches='tight')

