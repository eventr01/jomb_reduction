from __future__ import division
from tqdm import tqdm
import numpy as np
from functions_main import main_algo_LD, display_proba_MFPT, plot_figure
from parameters_toggleswitch import G, theta, K0, K1, D1, B, M, S, sigma, S0, S1, D0

from numba.errors import NumbaDeprecationWarning, NumbaPendingDeprecationWarning
import warnings
warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPendingDeprecationWarning)

### Simulation parameters
err = 0.001
tmax_simul = int(100/(np.min(D1)))
nb_step_simul = int(np.sum(B) * tmax_simul)
nb_simul = 500

t_simul = np.linspace(0, tmax_simul, nb_step_simul)
tmax = 10 * t_simul[-1]
nb_step = int(np.sum(B) * tmax)
t = np.linspace(0, tmax, nb_step)
max_iter = 100

### AMS parameters
nb_simul_MC = 250 # number of trajectories that are used for each Monte Carlo run
n_rep_AMS = 50 # number of replicates in each run of the AMS
nb_simul_AMS = 2 # number of repetition of the AMS algorithm
seuil_basin = 0 # the value of the score function on a saddle point (renormalized to 0 for the current algorithm)

### Specific parameters
Z_from = 2 # Initial basin
Z_to = 1 # Final basin
borne_eps_inf = 4 # lower epsilon used in the calculation
borne_eps_sup = 12 # upper epsilon used in the calculation
eps_max_MC = 7 # value of epsilon until which the algorithm tries to compute the proabilities and MFPTs with a crude MC method

### Large deviations algorithm

nb_Z, eq_st, mat_cout_LD, mat_id_traj_opt, mat_liste_traj_opt, eq_inst, eq_inst_liste = main_algo_LD(
                                                                G, theta, K0, K1, D1, B, M, S, sigma, err,
                                                                nb_simul, t_simul, tmax, nb_step, t, max_iter)

print(mat_cout_LD)


# List of quantities to fill as functions of epsilon

proba_LD = list()
proba_AMS = list()
proba_MC = list()
temps_retour_attracteur = list()
temps_sortie_bassin = list()
temps_sortie_attracteur = list()
temps_passage_MC = list()

### Main loop on epsilon

for eps in tqdm(range(borne_eps_inf, borne_eps_sup), desc = 'calcul eps'):

    mu_eq, kon_eq, proba_LD_eps, proba_MC_eps, proba_AMS_eps,\
    temps_retour_attracteur_eps, temps_sortie_attracteur_eps, \
    temps_sortie_bassin_eps, MFPT_MC_eps = \
                        display_proba_MFPT(Z_from, Z_to, eps, eps_max_MC, theta, K0, K1, D1, B, M, S, sigma, G,
                                           n_rep_AMS, nb_simul_MC, nb_simul_AMS, seuil_basin, err, nb_Z, 
                                           eq_st, nb_simul, t, mat_cout_LD, mat_id_traj_opt, eq_inst_liste)

    proba_LD.append(proba_LD_eps)
    proba_AMS.append(proba_AMS_eps)
    proba_MC.append(proba_MC_eps)

    temps_sortie_attracteur.append(temps_sortie_attracteur_eps)
    temps_sortie_bassin.append(temps_sortie_bassin_eps)
    temps_retour_attracteur.append(temps_retour_attracteur_eps)
    temps_passage_MC.append(MFPT_MC_eps)

### Plot results

plot_figure(eps_max_MC, borne_eps_inf, borne_eps_sup, proba_LD, proba_AMS, proba_MC,
                     temps_sortie_attracteur, temps_sortie_bassin, temps_retour_attracteur,
                     temps_passage_MC)

