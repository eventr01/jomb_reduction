from __future__ import division
import numpy as np
from scipy.integrate import odeint
from sympy import *
from numba import njit
from simulation_functions import simulate_full
from basic_functions import my_random_choice, kon, koff
from AMS_functions import update_timeandtau, update_vect
from lagrangian_functions import model_fluctuation_inv, model_relaxation, Lag_opt, Lagrangian, my_transform_fluctuation

def build_eqst(Eo, Po, t_simul, t, theta, D1, K0, K1, S, sigma, M, B, G):
    """This function simulates a cell modeled by the PDMP system, and get a relaxation trajectory from its final position
    until a small neighborhood of an attractor
    It returns the relaxation trajectory and the final position of the simulation"""
    cell_E, cell_P = simulate_full(Eo, Po, t_simul, theta, K0, K1, D1, B, M, S, sigma, G,
                                   kon, koff, my_random_choice, update_timeandtau, update_vect)
    sol = odeint(model_relaxation, cell_P[-1, :], t, args = (theta, D1, K0, K1, S, sigma, M, B, G))
    return sol, cell_P

### Functions useful for the second step described in Section 5.2, for recovering the saddle points
### of the deterministic system (4)

@njit()
def build_togd_ij(liste1, i, liste2, j, subdiv, err, theta, D1, K0, K1, S, sigma, M, B, G):
    """This functions takes as main entries two lists of attractors liste1 and liste2, and two indices i and j.
    It then realized the first step of the algorithm detailed in Appendix I, for finding the first point on the line
    (liste1[i], liste2[j]) where the sign of the derivative of the Lagrangian changes."""
    pts_init = liste1[i].copy()
    pts_fin = liste2[j].copy()
    pas = pts_fin - pts_init
    if np.sum(np.abs(pas)) > err:
        X_tmp = pts_init.copy()
        lag_tmp = Lag_opt(X_tmp, theta, D1, K0, K1, S, sigma, M, B, G)
        cnt = 0
        while (Lag_opt(X_tmp, theta, D1, K0, K1, S, sigma, M, B, G) >= lag_tmp and cnt < subdiv):
            lag_tmp = Lag_opt(X_tmp, theta, D1, K0, K1, S, sigma, M, B, G)
            X_tmp += (1/subdiv) * pas
            cnt += 1
        lag_tmp = Lag_opt(X_tmp, theta, D1, K0, K1, S, sigma, M, B, G)
        while (Lag_opt(X_tmp, theta, D1, K0, K1, S, sigma, M, B, G) <= lag_tmp and cnt < subdiv):
            lag_tmp = Lag_opt(X_tmp, theta, D1, K0, K1, S, sigma, M, B, G)
            X_tmp += (1/subdiv) * pas
            cnt += 1
        if cnt < subdiv :
            return X_tmp

def build_togd(liste1, liste2, subdiv, err, theta, D1, K0, K1, S, sigma, M, B, G):
    """This functions applies the previous function between any pair of equilibrium points. It returns a list of
    points where the sign of the derivative of the Lagrangian changes."""
    res = list()
    for i in range(0,len(liste1)):
        for j in range(0, len(liste2)):
            r = build_togd_ij(liste1, i, liste2, j, subdiv, err, theta, D1, K0, K1, S, sigma, M, B, G)
            if type(r) == np.ndarray:
                res.append(r)
    return res

def my_gradient_descent(err, max_iter, X_sy, mat_jac, mat_hess, Xo, theta, D1, K0, K1, S, sigma, M, B, G):
    """This function realized the gradient descent described on the second step of the algorithm described in Appendix I.
    From a point Xo, it returns a local extremum X of the Lagrangian, which may correspond to a saddle point"""
    nb_iter = 0
    X = Xo.copy()
    while Lag_opt(X, theta, D1, K0, K1, S, sigma, M, B, G) > err:
        if np.min(X) <= 0 or np.max(X) > 1 or nb_iter == max_iter:
            return np.zeros(G)
        jac_sub = Matrix(mat_jac.subs([(X_sy[j],X[j]) for j in range(0,G)]))
        hess_sub = Matrix(mat_hess.subs([(X_sy[j],X[j]) for j in range(0,G)])).reshape(G,G)
        tmp = hess_sub*jac_sub
        for i in range(0,G):
            d_opt = jac_sub[i]/tmp[i]
            X[i] = X[i] - d_opt*jac_sub[i]
        nb_iter += 1
    return X

def find_eqinst(X, X_real, pts, pts_ext, err, t, nb_step, tmax, theta, D1, K0, K1, S, sigma, M, B, G):
    """This function takes as main entry a list of list called 'pts' and such that:
    ### 0 - list of saddle points associated to an attractor by a fluctuation trajectory
    ### 1 - cost of the trajectory associated to each saddle point
    ### 2 - fluctuation trajectory
    ### 3 - saddle point associated to an attractor by a relaxation trajectory
    ### 4 - associated relaxation trajecory
    It returns the same list of list which has been extended by the values associated to the list of saddle points pts_ext"""
    sol_back = odeint(model_fluctuation_inv, X, t, args = (theta, D1, K0, K1, S, sigma, M, B, G))
    sol_for = odeint(model_relaxation, X, t, args = (theta, D1, K0, K1, S, sigma, M, B, G))
    for i in range(0,len(pts_ext)):
        if np.sum(np.abs(sol_back[-1, :G] - pts_ext[i])) < err:
            L = np.array([Lagrangian(sol_back[j, :], my_transform_fluctuation(sol_back[j, :],
               theta, D1, K0, K1, S, sigma, M, B, G), theta, D1, K0, K1, S, sigma, M, B, G) for j in range(0, nb_step)])
            cost = np.sum(L * tmax / nb_step)
            pts[i][0].append(X_real)
            pts[i][1].append(cost)
            pts[i][2].append(sol_back)
        if np.sum(np.abs(sol_for[-1, :] - pts_ext[i])) < err:
            pts[i][3].append(X_real)
            pts[i][4].append(sol_for)
    return pts


def build_eqinst(X_real, pts, pts_ext, mat, err, func1, t, nb_step, tmax, theta, D1, K0, K1, S, sigma, M, B, G):
    """This function takes as main entry a list of list called 'pts' and such that:
    ### 0 - list of saddle points associated to an attractor by a fluctuation trajectory
    ### 1 - cost of the trajectory associated to each saddle point
    ### 2 - fluctuation trajectory
    ### 3 - saddle point associated to an attractor by a relaxation trajectory
    ### 4 - associated relaxation trajecory
    It returns the same list of list which has been extended by the values associated to the list of saddle points pts_ext"""
    X = X_real.copy()
    ### On random directions, we plot reverse fluctuation trajectories
    for j in range(0, 2 ** G):
        tmp = list()
        for i in range(0, G):
            if mat[G-1][j,i] == 1:
                tmp.append(i)
        l = len(tmp)
        for i in range(0,2**l):
            for k in range(0,l):
                if mat[l-1][i,k] == 1:
                    X[tmp[k]] += err
                else:
                    X[tmp[k]] -= err
            pts = func1(X, X_real, pts, pts_ext, err, t, nb_step, tmax, theta, D1, K0, K1, S, sigma, M, B, G)
    ### We plot a reverse fluctuation trajectory on the naive direction
    for k in range(0,len(pts_ext)):
        X = X_real + err * (pts_ext[k] - X_real)
        pts = func1(X, X_real, pts, pts_ext, err, t, nb_step, tmax, theta, D1, K0, K1, S, sigma, M, B, G)
    return pts


def build_from_eqinst_to_eqst(pts_inst, num_bassin_init, num_bassin_fin, err):
    """Takes the list of list returned by the function build_eqinst, and get the coordinate of the optimal
    fluctuation trajectory between each pair of basin (id_min), and the associated saddle point (points_accessibles)"""
    points_accessibles = list()
    id_min = -1
    tmp_cout = float('inf')
    for n_init, x_inst_init in enumerate(pts_inst[num_bassin_init][0]):
        check = 0
        min_cout = pts_inst[num_bassin_init][1][n_init]
        for n_fin, x_inst_fin in enumerate(pts_inst[num_bassin_fin][3]):
            if np.sum(np.abs(x_inst_fin - x_inst_init)) < err:
                points_accessibles.append(n_init)
                check = 1
        if (check == 1) and (tmp_cout > min_cout):
            tmp_cout = min_cout
            id_min = n_init
    return id_min, points_accessibles

