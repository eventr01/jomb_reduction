from __future__ import division
import numpy as np
from scipy.stats import beta
import random
from numba import njit

### This file contains the basic functions of the model (kon, koff), and 3 basic functions that are used in the main files

@njit()
def kon(P, theta, K0, K1, S, sigma, M, G):
    """Interaction function of the model.
    Takes a vector of proteins P and return a vector containing the value of the interaction functions associated to P
     for each gene"""
    A = np.exp(theta)
    vP = np.diag(sigma) * P * np.ones(G)
    I = np.ones((G, G)) - np.diag(np.ones(G))
    X = (vP / S) ** M
    tmp = (I + A * X) / (1 + I * X)
    Phi = np.array([np.prod(tmp[i]) for i in range(0, G)])
    res = (K0 + K1 * Phi) / (1 + Phi)
    return res

@njit()
def koff(P, B):
    """Rate of the exponential law of jumping from state 'on' to state 'off' for a promoter. We decide to keep this
    function constant for the present work"""
    return B

@njit()
def my_random_choice(n,p):
    """Equivalent function to np.random.choice(), which is recoded for fitting with the numba package"""
    tmp = random.random()
    for i in range(0,n):
        if tmp >= np.sum(p[:i]) and tmp <= np.sum(p[:i+1]):
            return i
    return n-1

def build_matetat(ng):
    """Build a matrix of size (2^ng X ng) containing all the elements of (0,1)^ng"""
    res = np.zeros((2 ** ng, ng))
    for i in range(0, 2 ** ng):
        k = i
        j = ng - 1
        while k != 0:
            tmp = k % 2
            if (tmp == 1):
                res[i, j] = 1
            k = int(k / 2)
            j -= 1
    return res

def my_partie_entiere(x):
    """return the integer part of a real number"""
    if (x - int(x)) < 0.5:
        return int(x)
    return int(x) + 1

def prod_beta(X, A, B, D):
    res = 1
    for i in range(0,np.size(X)):
        res = res * beta.pdf(X[i], A[i] / D[i], B[i] / D[i])
    return res

def log_stationary_distribution(X, mu, kon_eq, koff, D):
    """return the beta mixture associated to the attractors (and their ratio)"""
    res = 0
    for z in range(0,len(mu)):
        res += mu[z]*prod_beta(X,kon_eq[z],koff,D)
    return np.log(res)