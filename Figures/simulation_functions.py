from __future__ import division
import numpy as np
from numba import njit
from AMS_functions import my_append_array, my_append_float

@njit()
def simulate_full(E, P, timepoints, theta, K0, K1, D, B, M, S, sigma, G, kon, koff, my_random_choice,
                  update_timeandtau, update_vect):
    """Exact simulation of the network theta when the behaviour of a cell is modeled by the PDMP system."""
    sim_E = np.zeros((len(timepoints),G))
    sim_P = np.zeros((len(timepoints),G))
    T = 0
    Told, E_old, P_old = T, E.copy(), P.copy()
    cnt = -1
    for t in timepoints:
        while (t >= T):
            Told, E_old, P_old = T, E.copy(), P.copy()
            ### 0. update time and vector
            T, U, tau = update_timeandtau(T, E, P, theta, K0, K1, B, M, S, sigma, G, kon, koff)
            E, P = update_vect(U, E, P, theta, K0, K1, D, B, M, S, sigma, G, tau, kon, koff, my_random_choice)

        P_old = E_old + (P_old - E_old)*np.exp((Told-t)*D)
        cnt += 1
        sim_E[cnt, :] = E_old
        sim_P[cnt, :] = P_old
    return sim_E, sim_P


@njit()
def simulate_until_crossing(sim_T, sim_E, sim_P, score_P, Z1, Z2, nb_Z, X_eq, bet_eq, mu_eq, kon_eq,
                            seuil_attracteur, seuil_basin, mat_correction_basins,
                            theta, K0, K1, D, B, M, S, sigma, G, kon, koff, my_random_choice, func_score,
                            update_timeandtau, update_vect, check_attractor):
    """Exact simulation of the network theta when the behaviour of a cell is modeled by the PDMP system.
    The simulation starts in basin Z1 and stops when a point of another basin is reached or a small neighborhood
    of the attractor of Z1 is reached.
    """
    T = sim_T[-1]
    E = sim_E[-1].copy()
    P = sim_P[-1].copy()
    score_P0 = func_score(X_eq[Z1], Z1, Z2, X_eq, bet_eq, mu_eq, kon_eq, koff(X_eq[Z1], B), D)
    still_in_game = 0

    while still_in_game == 0:
        T, U, tau = update_timeandtau(T, E, P, theta, K0, K1, B, M, S, sigma, G, kon, koff)
        run = check_attractor(U, E, P, X_eq[Z1], seuil_attracteur, D)
        E, P = update_vect(U, E, P, theta, K0, K1, D, B, M, S, sigma, G, tau, kon, koff, my_random_choice)

        vect_score = np.zeros(nb_Z)
        basin_actuel_dominant = np.zeros(nb_Z, dtype=np.int64)
        for Z in range(0, nb_Z):
            if Z != Z1:
                vect_score[Z] = func_score(P, Z1, Z, X_eq, bet_eq, mu_eq, kon_eq, koff(P, B), D)
                if vect_score[Z] >= seuil_basin + mat_correction_basins[Z1, Z]:
                    basin_actuel_dominant[Z] = 1

        sim_E = my_append_array(sim_E, E)
        sim_P = my_append_array(sim_P, P)
        sim_T = my_append_float(sim_T, T)
        score_P = my_append_float(score_P, vect_score[Z2])

        ### The simulation stops if it reaches another basin or come back to the neighborhood of the attractor of Z1
        ### still in game is equal to -1 if the simulation has not reached another basin
        if (np.sum(np.abs(P - X_eq[Z1])**2) <= seuil_attracteur) or run == 0:
            still_in_game = -1

        if (np.sum(basin_actuel_dominant) >= 1):
            ### still in game is equal to 1 if the basin that has been reached is the expected basin Z2
            if basin_actuel_dominant[Z2] == 1:
                still_in_game = 1
            ### still in game is equal to -2 if the basin that has been reached is not the expected basin Z2
            else:
                still_in_game = -2

    return still_in_game, sim_T, sim_E, sim_P, score_P

@njit()
def simulate_for_MFPT(E0, sim_P, score_P, score_obj, Z1, nb_Z, X_eq, bet_eq, mu_eq, kon_eq, seuil_basin,
                      mat_correction_basins, theta, K0, K1, D, B, M, S, sigma, G, kon, koff, my_random_choice, func_score,
                      update_timeandtau, update_vect):
    """Exact simulation of the network theta when the behaviour of a cell is modeled by the PDMP system.
        The simulation starts in basin Z1 and stops when a point of another basin is reached. It returns the basin that
        has been reached, the time of the simulation, the simulation itself (only the proteins, sim_P)
        and the score function on each point of the simulation (score_P)
        """
    T = 0.
    E = E0.copy()
    P = sim_P[-1].copy()
    still_in_basin = 1
    basin_final = Z1

    while still_in_basin:
        # Update time and vector
        T, U, tau = update_timeandtau(T, E, P, theta, K0, K1, B, M, S, sigma, G, kon, koff)
        E, P = update_vect(U, E, P, theta, K0, K1, D, B, M, S, sigma, G, tau, kon, koff, my_random_choice)

        vect_score = score_P[-1].copy()
        basin_actuel_dominant = np.zeros(nb_Z, dtype=np.int64)
        for Z in range(0, nb_Z):
            if Z != Z1:
                vect_score[Z] = func_score(P, Z1, Z, X_eq, bet_eq, mu_eq, kon_eq, koff(P, B), D)
                if vect_score[Z] >= score_obj[Z]:
                    basin_actuel_dominant[Z] = 1
                    basin_final = Z

        ### On arrete si on sort de Z1
        if np.sum(basin_actuel_dominant) >= 1:
            still_in_basin = 0

        sim_P = my_append_array(sim_P, P)
        score_P = my_append_array(score_P, vect_score)

    return basin_final, T, sim_P, score_P

@njit()
def simulate_for_convergence(E0, sim_P, score_P, score_obj, Z1, Z2, nb_Z, X_eq, bet_eq, mu_eq, kon_eq,
                        theta, K0, K1, D, B, M, S, sigma, G, kon, koff, my_random_choice, func_score,
                        update_timeandtau, update_vect):
    """Exact simulation of the network theta when the behaviour of a cell is modeled by the PDMP system.
        The simulation starts in a saddle point between Z2 and Z1. the variable "arrived" is equal to 1 if the simulation has reached
        the attractor of Z1, and to -1 if it has reached another basin. It returns the time of convergence
        (when 'arrived = 1')."""
    T = 0.
    E = E0.copy()
    P = sim_P[-1].copy()
    arrived = 0

    while arrived == 0:
        # Update time and vector
        T, U, tau = update_timeandtau(T, E, P, theta, K0, K1, B, M, S, sigma, G, kon, koff)
        E, P = update_vect(U, E, P, theta, K0, K1, D, B, M, S, sigma, G, tau, kon, koff, my_random_choice)

        vect_score = np.zeros(nb_Z)
        score = func_score(P, Z1, Z2, X_eq, bet_eq, mu_eq, kon_eq, koff(P, B), D)
        if score >= score_obj:
            arrived = 1
        for Z in range(0, nb_Z):
            if Z != Z2:
                vect_score[Z] = func_score(P, Z2, Z, X_eq, bet_eq, mu_eq, kon_eq, koff(P, B), D)
                if vect_score[Z] >= func_score(X_eq[Z], Z2, Z, X_eq, bet_eq, mu_eq, kon_eq, koff(X_eq[Z], B), D):
                    arrived = -1

        sim_P = my_append_array(sim_P, P)
        score_P = my_append_float(score_P, score)

    return arrived, T, sim_P, score_P
