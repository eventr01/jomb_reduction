from __future__ import division
import numpy as np

### This file contains functions that are used to filter lists that are used in the main files

def my_filtre(liste, err):
    """Take a list and remove all similar elements with a level of precision equal to err"""
    res = list()
    for i in range(0, len(liste)):
        cnt = 0
        for j in range(0, i):
            if np.sum(np.abs(liste[i] - liste[j])) < err:
                cnt = 1
                break
        if cnt == 0:
            res.append(liste[i])
    return res

def my_filtre_similar_2(liste1, liste2, err):
    """Take a list and remove all similar elements with a level of precision equal to err
    Apply the same transformation to the second list"""
    res1 = list()
    res2 = list()
    for i in range(0, len(liste1)):
        cnt = 0
        for j in range(0, i):
            if np.sum(np.abs(liste1[i] - liste1[j])) < err:
                cnt = 1
                break
        if cnt == 0:
            res1.append(liste1[i])
            res2.append(liste2[i])
    return res1, res2

def my_filtre_similar_3(liste1, liste2, liste3, err):
    """Take a list and remove all similar elements with a level of precision equal to err
    Apply the same transformation to the second and the third lists"""
    res1 = list()
    res2 = list()
    res3 = list()
    for i in range(0, len(liste1)):
        cnt = 0
        for j in range(0, i):
            if np.sum(np.abs(liste1[i] - liste1[j])) < err:
                cnt = 1
                break
        if cnt == 0:
            res1.append(liste1[i])
            res2.append(liste2[i])
            res3.append(liste3[i])
    return res1, res2, res3


def my_order_comp(tab1,tab2):
    """Take 2 lists of array, order the first one with respect to the sum of each elements, and apply the same
    transformations to the second one
    """
    nb = len(tab1)
    for j in range(0,nb):
        tmp1 = tab1[j].copy()
        tmp2 = tab2[j]
        for i in range(j+1,nb):
            if np.sum(tab1[j]) > np.sum(tab1[i]):
                tab1[j][:], tab2[j] = tab1[i][:], tab2[i]
                tab1[i][:], tab2[i] = tmp1[:], tmp2
                tmp1[:], tmp2 = tab1[j][:], tab2[j]
    return tab1, tab2


def my_filtre_ratio(liste, err):
    """Take a list and remove all similar elements with a level of precision of err, returns the list filtered
    and the ratio of counts of each elements in the initial list"""
    l = len(liste)
    res1 = list()
    res2 = list()
    for i in range(0, len(liste)):
        add = 0
        for j in range(0, i):
            if (np.sum(np.abs(liste[i] - liste[j])) < err):
                add = 1
                break
        if add == 0:
            cnt = 1.
            res1.append(liste[i])
            for k in range(i+1, l):
                if np.sum(np.abs(liste[i] - liste[k])) < err:
                    cnt += 1
            res2.append(cnt/l)
    return res1, res2


def my_filtre_comp(liste1, liste2, err):
    """Take 2 lists, and return elements of the first list that are not in the second one (with a level of precision
    equal to err)"""
    res = list()
    for i in range(0, len(liste1)):
        cnt = 0
        for j in range(0, len(liste2)):
            if np.sum(np.abs(liste1[i] - liste2[j])) < err:
                cnt = 1
                break
        if cnt == 0:
            res.append(liste1[i])
    return res



