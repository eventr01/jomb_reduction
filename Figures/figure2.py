from __future__ import division
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from simulation_functions import simulate_full
from AMS_functions import update_timeandtau, update_vect
from basic_functions import kon, koff, my_random_choice
from lagrangian_functions import model_relaxation

### Figure 2

### NETWORK PARAMETERS

G = 4 # Number of genes
S0 = 1*np.ones(G) # mRNA creation rates
D0 = 1*np.ones(G) # mRNA degradation rates
S1 = 1*np.ones(G) # Protein creation rates
D1 = 0.2*np.ones(G) # Protein degradation rates

K0 = (1/80) * np.ones(G) # basal promoter frequency
K1 = (31/80) * np.ones(G) # highest promoter frequency
B = (10/8) * np.ones(G) # burst size
M = 3. * np.ones((G,G)) # order of the interaction between Transcription factor and promoter
sigma = np.outer(np.ones(G), S0*S1/(D0*D1)) # ratio coming from the full model described in Appendix A

theta = np.array([[100, 100, 100, 100], [10,2,0,0],[0,10,2,0],[0,0,10,2]])

# Thresold value
S = sigma.copy()
for i in range(0,G):
    for j in range(0,G):
        if i != j:
            S[j,i] = sigma[j,i] * np.exp(-theta[i,i]/M[i,i])

# PARAMETERS FOR SIMULATION
t_max = 100
nb_step = 10 * t_max
t_simul = np.linspace(0, t_max, nb_step)
nb_cells = 100

# INITIAL CONDITIONS
P_init = 0.05
P0 = P_init * np.ones(G)
E0 = np.zeros(G)

eps = 7
cells_mean = np.zeros((nb_cells, nb_step, G))
for i in range(0, nb_cells):
    cell_E, cell_P = simulate_full(E0, P0, t_simul, theta, K0 * eps, K1 * eps, D1, B * eps, M, S, sigma, G, kon, koff, my_random_choice,
                  update_timeandtau, update_vect)
    cells_mean[i, :, :] = cell_P[:, :]
cell_eps1 = np.mean(cells_mean, axis=0)

eps = 30
cells_mean = np.zeros((nb_cells, nb_step, G))
for i in range(0, nb_cells):
    cell_E, cell_P = simulate_full(E0, P0, t_simul, theta, K0 * eps, K1 * eps, D1, B * eps, M, S, sigma, G, kon, koff, my_random_choice,
                  update_timeandtau, update_vect)
    cells_mean[i, :, :] = cell_P[:, :]
cell_eps2 = np.mean(cells_mean, axis=0)

cell_det = odeint(model_relaxation, P0, t_simul, args = (theta, D1, K0, K1, S, sigma, M, B, G))


fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15,5))

line0, = ax[1].plot(t_simul, cell_eps1[:, 1],  color='blue', alpha=1)
line1, = ax[1].plot(t_simul, cell_eps1[:, 2],  color='orange', alpha=1)
line2, = ax[1].plot(t_simul, cell_eps1[:, 3],  color='green', alpha=1)
line3, = ax[2].plot(t_simul, cell_eps2[:, 1],  color='blue', alpha=1)
line4, = ax[2].plot(t_simul, cell_eps2[:, 2],  color='orange', alpha=1)
line5, = ax[2].plot(t_simul, cell_eps2[:, 3],  color='green', alpha=1)
line6, = ax[0].plot(t_simul, cell_det[:, 1],  color='blue', alpha=1)
line7, = ax[0].plot(t_simul, cell_det[:, 2],  color='orange', alpha=1)
line8, = ax[0].plot(t_simul, cell_det[:, 3],  color='green', alpha=1)
ax[1].set_xlabel('time')
ax[2].set_xlabel('time')
ax[0].set_xlabel('time')
ax[0].legend([line3, line4, line5], ['gene 1', 'gene 2', 'gene 3'])
ax[0].set_ylabel('gene expression (protein concentration)')
plt.show()

