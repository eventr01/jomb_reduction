from __future__ import division
import matplotlib.pyplot as plt
import numpy as np
from scipy.special import beta
from functions_main import main_algo_LD
from AMS_functions import score_function, update_timeandtau, update_vect, bernouilli_func
from simulation_functions import simulate_for_MFPT
from basic_functions import kon, koff, my_random_choice
from parameters_toggleswitch import G, theta, K0, K1, D1, B, M, S, sigma, S0, S1, D0

from numba.errors import NumbaDeprecationWarning, NumbaPendingDeprecationWarning
import warnings
warnings.simplefilter('ignore', category=NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=NumbaPendingDeprecationWarning)

### FIGURE 4

### SIMULATION PARAMETERS
err = 0.001
tmax_simul = int(10/(np.min(D1)))
nb_step_simul = int(np.sum(B) * tmax_simul)
nb_simul = 200
nb_simul_MC = 3000

eps = 5
K0_eps = K0 * eps
K1_eps = K1 * eps
B_eps = B * eps

t_simul = np.linspace(0, tmax_simul, nb_step_simul)
tmax = 10 * t_simul[-1]
nb_step = int(np.sum(B) * tmax)
t = np.linspace(0, tmax, nb_step)
max_iter = 100

### AMS PARAMETERS
seuil_basin = 0

### Specific parameters
tot = 500 # the mean will be computed on 50 trajectories of exit
Z_from = 1 # Initial basin
Z_to = 2 # Final basin


### Find equilibrium points, number of basins, saddle points, exit trajectories and exit costs

nb_Z, eq_st, mat_cout_LD, mat_id_traj_opt, mat_liste_traj_opt, eq_inst, eq_inst_liste = main_algo_LD(
                                                                G, theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, err,
                                                                nb_simul, t_simul, tmax, nb_step, t, max_iter)

x_inst = eq_inst_liste[Z_from][0][mat_id_traj_opt[Z_from, Z_to]] # coordinate of the saddle point between basin Z_from and basin Z_to

kon_eq = np.array([kon(eq_st[z], theta, K0_eps, K1_eps, S, sigma, M, G) for z in range(0, nb_Z)])
bet_eq = np.ones(nb_Z)
for z in range(0, nb_Z):
    bet_eq[z] = np.prod(beta(kon_eq[z] / D1, koff(eq_st[z], B_eps) / D1))
mu_eq = np.ones(nb_Z)

# BUILD THE MATRIX OF THE SCORE FUNCTION ON EVERY SADDLE POINTS

mat_score_saddle_point = np.zeros((nb_Z, nb_Z))
for k in range(0, nb_Z):
    for l in range(0, nb_Z):
        if mat_id_traj_opt[k, l] > -1:
            x_inst = eq_inst_liste[k][0][mat_id_traj_opt[k, l]]
            mat_score_saddle_point[k, l] = score_function(
                x_inst, k, l, eq_st, bet_eq, mu_eq, kon_eq, koff(x_inst, B_eps), D1)


MFPT_MC = list()

P0_MFPT = np.array([eq_st[1]])
score0_MFPT = np.array([np.zeros(nb_Z)])
score_obj_MFPT = np.zeros(nb_Z)
for Z in range(0, nb_Z):
    score0_MFPT[0][Z] = score_function(eq_st[1], Z_from, Z, eq_st, bet_eq, mu_eq, kon_eq, koff(eq_st[Z], B_eps), D1)
    score_obj_MFPT[Z] = score_function(eq_st[Z], Z_from, Z, eq_st, bet_eq, mu_eq, kon_eq, koff(eq_st[Z], B_eps), D1)

cnt = 0
while cnt < tot:
    sim_P0 = P0_MFPT.copy()
    score_P0 = score0_MFPT.copy()
    E0 = bernouilli_func(eq_st[1])
    num_basin, T, sim_P, score_P = simulate_for_MFPT(E0, sim_P0, score_P0, score_obj_MFPT, Z_from, nb_Z, eq_st, bet_eq,
                                                     mu_eq, kon_eq, seuil_basin, mat_score_saddle_point,
                                                     theta, K0_eps, K1_eps, D1, B_eps, M, S, sigma, G,
                                                     kon, koff, my_random_choice, score_function,
                                                     update_timeandtau, update_vect)
    if num_basin == Z_to:
        MFPT_MC.append(T)
        cnt += 1

# PLOT THE RESULTS FOR THE TRANSITION BETWEEN BASIN 1 AND BASIN 2

fig1, ax1 = plt.subplots(nrows=1, ncols=2, figsize=(10, 5))
exp = np.random.exponential(np.mean(MFPT_MC), tot)
ax1[0].hist(MFPT_MC, bins=50, density=True, color='grey', alpha=0.7, label='empirical distribution')
ax1[1].hist(np.log(MFPT_MC), bins=50, density=True, color='grey', alpha=0.7)
x = np.linspace(0, np.max(MFPT_MC), tot)
y = (1/np.mean(MFPT_MC)) * np.exp(- (1/np.mean(MFPT_MC)) * x)

x_log = np.log(x)
y_log = y.copy()

for i in range(0, tot - 1):
    y_log[i] = y[i] * (x[i+1] - x[i]) / (x_log[i+1] - x_log[i])

line, = ax1[0].plot(x, y, color='red', label='exponential fit')
ax1[1].plot(x_log, y_log, color='red')
ax1[0].set_xlabel('time(h)')
ax1[0].set_ylabel('f(T)')
ax1[1].set_xlabel('log(T)')
ax1[1].set_ylabel('f(log(T))')
ax1[0].legend()
plt.show()

