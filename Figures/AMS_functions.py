import numpy as np
import math as mt
import random
from numba import njit

### This file contains all the functions that are used for the AMS algorithm, except the functions used for
# simulating the PDMP system, which can be found in the file simulation_file

###  Two technical functions used for managing memory when applying the AMS with numba

@njit()
def my_append_float(arr1, value):
    finalArray = np.zeros(len(arr1) + 1, dtype=np.float64)
    finalArray[:-1] = arr1
    finalArray[-1] = value
    return finalArray

@njit()
def my_append_array(arr1, value):
    dim = arr1.shape
    finalArray = np.zeros((dim[0] + 1, dim[1]), dtype=np.float64)
    finalArray[:-1] = arr1
    finalArray[-1] = value
    return finalArray

## Functions used for the core of the AMS algorithm

@njit()
def bernouilli_func(P):
    """Returns a vector of promoters chosen under a vector of Bernouilli laws of parameter P"""
    res = np.zeros(len(P))
    for i in range(0, len(P)):
        tmp = random.random()
        if tmp < P[i]:
            res[i] = 1
    return res

@njit()
def score_function(X, Z1, Z2, X_eq, bet_eq, mu_eq, kon_eq, koff, D):
    """This is the score function described in Appendix G, for the pair of basins (Z1, Z2)"""
    nb_Z = len(kon_eq)
    res_x = np.ones(nb_Z)

    for Z in range(0, nb_Z):
        res_x[Z] = np.prod(X ** (kon_eq[Z] / D)) / bet_eq[Z]
    res_max = np.max(res_x)

    if res_max == res_x[Z2]:
        res = - np.log(res_x[Z1] / res_max)
    else:
        res = np.log(res_x[Z2] / res_max)
    return 100 * res

def find_seuil_attracteur(X_st, nb_simul, theta, K0, K1, D, B, M, S, sigma, G, func_bernouilli, kon, koff):
    """This functions returns the radius of a neighborhood around an attractor X_st that corresponds to one step
    of the simulation of the PDMP system, using the method of the file simulation_functions"""
    seuil = np.zeros(nb_simul)
    for i in range(nb_simul):
        E = func_bernouilli(X_st)
        tau = np.sum((1 - E) * np.max(kon(X_st, theta, K0, K1, S, sigma, M, G)) + E * koff(X_st, B))
        U = np.random.exponential(scale=1 / tau)
        seuil[i] = np.sum(((X_st - E) * (1 - np.exp(- U * D)))**2)
    res = np.quantile(seuil, 0.9)/2
    return res

### These three functions are used for the simulation of the AMS trajectories

@njit()
def update_vect(U, E, P, theta, K0, K1, D, B, M, S, sigma, G, tau, kon, koff, my_random_choice):
    ### 1. Update the continuous states
    P = E + (P - E) * np.exp(-U * D)
    ### 2. Update the promoters
    a, b = kon(P, theta, K0, K1, S, sigma, M, G) / tau, koff(P, B) / tau
    v = np.zeros(G + 1)  # Probabilities for possible transitions
    v[1:] = a * (1 - E) + b * E  # i = 0, ..., n-1 : switch promoter i
    v[0] = 1 - np.sum(v[1:])  # i = -1 : no change (phantom jump)
    i = my_random_choice(G + 1, v) - 1
    if i > -1:
        E[i] = 1 - E[i]

    return E, P

@njit()
def update_timeandtau(T, E, P, theta, K0, K1, B, M, S, sigma, G, kon, koff):
    # tau = np.sum((1 - E) * np.max(kon(P, theta, K0, K1, S, sigma, M, G)) + E * koff(P, B))
    tau = np.sum(koff(P, B))
    ### 0. Draw the waiting time before the next jump
    U = np.random.exponential(scale=1 / tau)
    T += U

    return T, U, tau


@njit()
def check_attractor(U, E, P, X_equilibrium, seuil, D):
    """Check if an attractor has been reached between two timepoints of the simulation (see Appendix G)"""
    run = 1
    tmp = U + 1
    denum = np.sum((X_equilibrium - E) * (P - E))
    if denum > 0:
        tmp = np.log(2 * np.sum((P - E) ** 2) / denum) / np.max(D)
    if tmp > 0 and U > tmp:
        P_test = E + (P - E) * np.exp(-tmp * D)
        if (np.sum((P_test - X_equilibrium) ** 2) <= seuil):
            run = 0

    return run

### CORE OF THE AMS ALGORITHM

@njit()
def find_position_first_exit(sim_E, sim_P, score_P, Z1, Z2, nb_Z, X_eq, bet_eq, mu_eq, kon_eq,
                            seuil_attracteur, seuil_AMS, mat_correction_basins, theta, K0, K1, D, B, M, S, sigma, G,
                            kon, koff, my_random_choice, func_score, update_timeandtau, update_vect):
    """Simulate until the first exit of the neighborhood of the attactor $\Gamma_{Z_i}$ for starting the AMS
    (see Appendix G)"""
    run = 1
    T = 0.
    score = score_P[-1]
    E = sim_E[-1].copy()
    P = sim_P[-1].copy()
    score_P0 = score
    E0 = E.copy()

    while run:
        T, U, tau = update_timeandtau(T, E, P, theta, K0, K1, B, M, S, sigma, G, kon, koff)
        E, P = update_vect(U, E, P, theta, K0, K1, D, B, M, S, sigma, G, tau, kon, koff, my_random_choice)

        vect_score = np.zeros(nb_Z)
        basin_actuel_dominant = np.zeros(nb_Z, dtype=np.int64)
        for Z in range(0, len(kon_eq)):
            if Z != Z1:
                vect_score[Z] = func_score(P, Z1, Z, X_eq, bet_eq, mu_eq, kon_eq, koff(P, B), D)
                if vect_score[Z] >= seuil_AMS + mat_correction_basins[Z1, Z]:
                    basin_actuel_dominant[Z] = 1

        sim_E = my_append_array(sim_E, E)
        sim_P = my_append_array(sim_P, P)
        score_P = my_append_float(score_P, vect_score[Z2])

        if np.sum(basin_actuel_dominant) >= 1:
            T = 0
            P = X_eq[Z1].copy()
            E = E0.copy()

        if np.sum((P - X_eq[Z1]) ** 2) > seuil_attracteur:
            score = func_score(P, Z1, Z2, X_eq, bet_eq, mu_eq, kon_eq, koff(P, B), D)
            if score >= score_P0:
                run = 0

    return T, sim_E, sim_P, score_P

@njit()
def find_starting_point(score_P, score_min):
    """Find the first point on a trajectory, the score of which which is greater than a minimal score"""
    i = 0
    while score_P[i] <= score_min:
        i += 1
    return i

@njit()
def AMS_run_trajectory(sim_T, sim_E, sim_P, score_P, nb_Z, X_eq, bet_eq, mu_eq, kon_eq, Z1, Z2,
                        seuil_attracteur, seuil_AMS, mat_correction_basins, theta, K0, K1, D, B, M, S, sigma, G,
                        kon, koff, my_random_choice, func_score, update_timeandtau, update_vect, check_attractor):
    """Exact simulation of the network theta when the behaviour of a cell is modeled by the PDMP system.
    The simulation stops when the trajectory reaches a small neighborhood of the attractor or changes basin."""
    T = sim_T[-1]
    E = sim_E[-1].copy()
    P = sim_P[-1].copy()

    run = 1
    while run:
        T, U, tau = update_timeandtau(T, E, P, theta, K0, K1, B, M, S, sigma, G, kon, koff)
        run = check_attractor(U, E, P, X_eq[Z1], seuil_attracteur, D)
        E, P = update_vect(U, E, P, theta, K0, K1, D, B, M, S, sigma, G, tau, kon, koff, my_random_choice)

        vect_score = np.zeros(nb_Z)
        dominant_basin = np.zeros(nb_Z, dtype=np.int64)
        for Z in range(0, len(kon_eq)):
            if Z != Z1:
                vect_score[Z] = func_score(P, Z1, Z, X_eq, bet_eq, mu_eq, kon_eq, koff(P, B), D)
                if vect_score[Z] >= seuil_AMS + mat_correction_basins[Z1, Z]:
                    dominant_basin[Z] = 1

        sim_E = my_append_array(sim_E, E)
        sim_P = my_append_array(sim_P, P)
        sim_T = my_append_float(sim_T, T)
        score_P = my_append_float(score_P, vect_score[Z2])

        ### We stop if the trajectory reaches the attractor or another basin
        if (np.sum((P - X_eq[Z1])**2) <= seuil_attracteur) or (np.sum(dominant_basin) >= 1):
            run = 0

    return sim_T, sim_E, sim_P, score_P


def AMS_step(traj_T, traj_E, traj_P, score_traj_P, max_score, n_rep, score_min,
             nb_Z, X_eq, bet_eq, mu_eq, kon_eq, Z1, Z2, seuil_attracteur, mat_correction_basins, seuil_AMS,
             theta, K0, K1, D, B, M, S, sigma, G,
             kon, koff, my_random_choice, func_score, update_timeandtau, update_vect, check_attractor):

    """One step of the AMS algorithm, where n_rep trajectories are simulated until being retired (when they reach the
    small naighborhood of the attractor of Z1 or another basin than Z2, or the basin Z2."""

    tmp_traj_possible_to_continue = max_score > score_min
    cnt_retired = n_rep - np.sum(tmp_traj_possible_to_continue)

    if cnt_retired < n_rep:
        traj_possible_to_continue = np.zeros(n_rep - cnt_retired, dtype=np.int64)
        traj_to_replace = np.zeros(cnt_retired, dtype=np.int64)
        cnt = 0
        for i in range(0, n_rep):
            if tmp_traj_possible_to_continue[i]:
                traj_possible_to_continue[cnt] = i
                cnt += 1
            else:
                traj_to_replace[i - cnt] = i

        for i in range(0, cnt_retired):

            traj_to_continue = np.random.choice(traj_possible_to_continue)
            from_point = find_starting_point(np.array(score_traj_P[traj_to_continue]), score_min)
            sim_T = traj_T[traj_to_continue][:from_point + 1]
            sim_E = traj_E[traj_to_continue][:from_point + 1]
            sim_P = traj_P[traj_to_continue][:from_point + 1]
            score_P = score_traj_P[traj_to_continue][:from_point + 1]

            sim_T, sim_E, sim_P, score_P = AMS_run_trajectory(sim_T, sim_E, sim_P, score_P, nb_Z, X_eq, bet_eq, mu_eq, kon_eq,
                                                    Z1, Z2, seuil_attracteur, seuil_AMS,
                                                    mat_correction_basins, theta, K0, K1, D, B, M, S, sigma, G,
                                                    kon, koff, my_random_choice, func_score,
                                                    update_timeandtau, update_vect, check_attractor)

            traj_T[traj_to_replace[i]] = sim_T
            traj_E[traj_to_replace[i]] = sim_E
            traj_P[traj_to_replace[i]] = sim_P
            score_traj_P[traj_to_replace[i]] = score_P
            max_score[traj_to_replace[i]] = np.max(score_P)

        score_min = np.min(max_score)

    else:
        score_min = float('inf')

    return score_min, cnt_retired, traj_T, traj_E, traj_P, score_traj_P, max_score


def init_AMS(sim_T0, sim_E0, sim_P0, score_P0, nb_Z,
             X_eq, bet_eq, mu_eq, kon_eq, Z1, Z2, seuil_attracteur, mat_correction_basins, seuil_AMS, n_rep,
             theta, K0, K1, D, B, M, S, sigma, G,
             kon, koff, my_random_choice, func_score, update_timeandtau, update_vect, check_attractor):
    """Initialize the AMS"""
    traj_T, traj_E, traj_P, score_traj_P = [], [], [], []
    max_score = np.zeros(n_rep)

    for i in range(0,n_rep):
        sim_T = sim_T0.copy()
        sim_E = sim_E0.copy()
        sim_P = sim_P0.copy()
        score_P = score_P0.copy()
        sim_T, sim_E, sim_P, score_P = AMS_run_trajectory(sim_T, sim_E, sim_P, score_P, nb_Z, X_eq, bet_eq, mu_eq, kon_eq,
                                                    Z1, Z2, seuil_attracteur, seuil_AMS, mat_correction_basins,
                                                    theta, K0, K1, D, B, M, S, sigma, G,
                                                    kon, koff, my_random_choice, func_score,
                                                    update_timeandtau, update_vect, check_attractor)
        traj_T.append(sim_T)
        traj_E.append(sim_E)
        traj_P.append(sim_P)
        score_traj_P.append(score_P)
        max_score[i] = np.max(score_P)

    score_min = np.min(max_score)

    return score_min, traj_T, traj_E, traj_P, score_traj_P, max_score

def main_AMS(sim_T0, sim_E0, sim_P0, score_P0, Z1, Z2, nb_Z, X_eq, bet_eq, mu_eq, kon_eq, mat_correction_basins,
             seuil_attracteur, seuil_AMS, n_rep, theta, K0, K1, D, B, M, S, sigma, G,
             kon, koff, my_random_choice, func_score, update_timeandtau, update_vect, check_attractor):
    """Main function of the AMS algorithm described in Appendix G.
    Return the probability associated to 1 run of the AMS, and the associated trajectories (including timepoints)"""
    proba = 1
    Q_AMS = 0

    score_min, traj_T, traj_E, traj_P, score_traj_P, max_score = init_AMS(sim_T0, sim_E0, sim_P0, score_P0,
                                nb_Z, X_eq, bet_eq, mu_eq, kon_eq, Z1, Z2,
                                seuil_attracteur, mat_correction_basins, seuil_AMS, n_rep, theta, K0, K1, D, B, M, S, sigma, G,
                                kon, koff, my_random_choice, func_score, update_timeandtau, update_vect, check_attractor)

    while score_min < seuil_AMS + mat_correction_basins[Z1, Z2]:
        Q_AMS += 1
        score_min, cnt_retired, traj_T, traj_E, traj_P, score_traj_P, max_score = AMS_step(
                                traj_T, traj_E, traj_P, score_traj_P, max_score, n_rep, score_min,
                                nb_Z, X_eq, bet_eq, mu_eq, kon_eq, Z1, Z2, seuil_attracteur, mat_correction_basins, seuil_AMS,
                                theta, K0, K1, D, B, M, S, sigma, G, kon, koff, my_random_choice, func_score,
                                update_timeandtau, update_vect, check_attractor)

        proba = proba * (1 - cnt_retired / n_rep)

    return proba, traj_T, traj_E, traj_P, score_traj_P



