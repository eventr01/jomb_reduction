from __future__ import division
import numpy as np
from numba import njit
from basic_functions import kon, koff

### Ths file contains the coming from the Large deviations analysis: Lagrangian, Hamiltonian, and drifts of the 
### relaxation and fluctuation trajectories

@njit()
def Lagrangian(X, V, theta, D1, K0, K1, S, sigma, M, B, G):
    return np.sum((1 / D1) * (np.sqrt(koff(X, B) * (V + D1 * X)) - np.sqrt(kon(X, theta, K0, K1, S, sigma, M, G) * (D1 * (1 - X) - V))) ** 2)

@njit()
def Hamiltonian(X, P, theta, D1, K0, K1, S, sigma, M, B, G):
    return np.sum(0.5 * (D1 * P * (1 - 2 * X) - (kon(X, theta, K0, K1, S, sigma, M, G) + koff(X, B)) + np.sqrt(
        (D1 * P + kon(X, theta, K0, K1, S, sigma, M, G) - koff(X, B)) ** 2 + 4 * kon(X, theta, K0, K1, S, sigma, M, G) * koff(X, B))))

@njit()
def my_transform_fluctuation(X, theta, D1, K0, K1, S, sigma, M, B, G):
    return D1 * (X * (kon(X, theta, K0, K1, S, sigma, M, G) + koff(X, B)) - kon(X, theta, K0, K1, S, sigma, M, G)) \
           / (kon(X, theta, K0, K1, S, sigma, M, G) * (1 - X) / X + koff(X, B) * X / (1 - X))

@njit()
def my_transform_fluctuation_inv(X, theta, D1, K0, K1, S, sigma, M, B, G):
    return -D1 * (X * (kon(X, theta, K0, K1, S, sigma, M, G) + koff(X, B)) - kon(X, theta, K0, K1, S, sigma, M, G)) \
           / (kon(X, theta, K0, K1, S, sigma, M, G) * (1 - X) / X + koff(X, B) * X / (1 - X))

@njit()
def my_transform_relaxation(X, theta, D1, K0, K1, S, sigma, M, B, G):
    return D1 * (kon(X, theta, K0, K1, S, sigma, M, G) / (kon(X, theta, K0, K1, S, sigma, M, G) + koff(X, B)) - X)

@njit()
def my_transform_relaxation_inv(X, theta, D1, K0, K1, S, sigma, M, B, G):
    return - D1 * (kon(X, theta, K0, K1, S, sigma, M, G) / (kon(X, theta, K0, K1, S, sigma, M, G) + koff(X, B)) - X)

@njit()
def Lag_opt(X, theta, D1, K0, K1, S, sigma, M, B, G):
    kon_tmp = kon(X, theta, K0, K1, S, sigma, M, G)
    koff_tmp = koff(X, B)
    return np.sum((kon_tmp * (1 - X) - koff_tmp * X) ** 2 / (kon_tmp * (1 - X) ** 2 + koff_tmp * X ** 2))
    # return np.sum(kon_tmp + koff_tmp -
    #         kon_tmp*koff_tmp/(kon_tmp*(1-X)**2 + koff_tmp*X**2))

def model_fluctuation(X, t, theta, D1, K0, K1, S, sigma, M, B, G):
    return my_transform_fluctuation(X, theta, D1, K0, K1, S, sigma, M, B, G)

def model_fluctuation_inv(X, t, theta, D1, K0, K1, S, sigma, M, B, G):
    return my_transform_fluctuation_inv(X, theta, D1, K0, K1, S, sigma, M, B, G)

def model_relaxation(X, t, theta, D1, K0, K1, S, sigma, M, B, G):
    return my_transform_relaxation(X, theta, D1, K0, K1, S, sigma, M, B, G)

def model_relaxation_inv(X, t, theta, D1, K0, K1, S, sigma, M, B, G):
    return my_transform_relaxation_inv(X, theta, D1, K0, K1, S, sigma, M, B, G)

### Non-optimized functions for derivative in the package SYMPY

def kon_for_deriv(X, theta, D1, K0, K1, S, sigma, M, B, G):
    A= np.exp(theta)
    vX = np.diag(sigma) * X * np.ones(G)
    I = np.ones((G, G)) - np.diag(np.ones(G))
    X = (vX / S) ** M
    Phi = np.prod((I + A*X)/(1 + I*X), axis=1)
    res = (K0 + K1*Phi)/(1 + Phi)
    return res

def koff_for_deriv(P, B):
    return B

def Lag_opt_for_deriv(X, theta, D1, K0, K1, S, sigma, M, B, G):
    kon_tmp = kon_for_deriv(X, theta, D1, K0, K1, S, sigma, M, B, G)
    koff_tmp = koff_for_deriv(X, B)
    return np.sum((kon_tmp * (1-X) - koff_tmp * X)**2 / (kon_tmp * (1 - X) ** 2 + koff_tmp * X ** 2))
