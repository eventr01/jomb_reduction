from __future__ import division
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
from scipy.integrate import odeint
import scipy.stats as st
from filtre_functions import my_order_comp, my_filtre_ratio
from lagrangian_functions import model_relaxation
from AMS_functions import update_timeandtau, update_vect
from simulation_functions import simulate_full
from basic_functions import kon, koff, my_random_choice
from parameters_toggleswitch import G, theta, K0, K1, D1, B, M, S, sigma, S0, S1, D0

### FIGURE 5

# SIMULATION PARAMETERS

eps = 8

err = 0.001
nb_simul = 100
t_max = 250
nb_step = 10 * t_max
t_simul = np.linspace(0, t_max, nb_step)
n_tot = 10 * nb_simul

# INITIAL CONDITIONS
P_init = 5e-2
P0 = P_init * np.ones(G)
E0 = np.zeros(G)

eq_st = list()
data = np.zeros((nb_simul, G))

xmax, ymax = .5, .5
xmin, ymin = 0, 0

fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15, 5), dpi=100)
ax[0].set_xlabel('gene 1')
ax[0].set_ylabel('gene 2')
ax[0].set_xlim(xmin,xmax)
ax[0].set_ylim(ymin,ymax)
ax[1].set_xlabel('gene 1')
ax[1].set_ylabel('gene 2')
ax[1].set_xlim(xmin,xmax)
ax[1].set_ylim(ymin,ymax)
ax[2].set_xticks([])
ax[2].set_xlabel('$Z_{--}$                 $Z_{+-}$                 $Z_{-+}$')
ax[2].set_ylabel('number of cells')
ax[2].set_ylim(0, n_tot)

# Plot the deterministic trajectory between 100 cells and the attractor of the basin they belong to

for i in range(0, nb_simul):
    cell_E, cell_P = simulate_full(E0, P0, t_simul, theta, K0 * eps, K1 * eps, D1, B * eps, M, S, sigma, G, kon, koff, my_random_choice,
                  update_timeandtau, update_vect)
    data[i, :] = cell_P[-1, :]
    sim = odeint(model_relaxation, cell_P[-1], t_simul, args=(theta, D1, K0, K1, S, sigma, M, B, G))
    eq_st.append(sim[-1, :])
    ax[0].plot(sim[:, 0], sim[:, 1], linewidth=0.5, color='grey', alpha=0.5)
    ax[0].scatter(sim[0, 0], sim[0, 1], c='black', marker='o', s=5, alpha=1)


eq_st, mu_eq = my_filtre_ratio(eq_st, err)
eq_st, mu_eq = my_order_comp(eq_st, mu_eq)
nb_Z = len(eq_st)

liste_color = ['blue','green','red']

if eq_st[1][0] > eq_st[1][1]:
    liste_annotation = ['$Z_{--}$', '$Z_{+-}$', '$Z_{-+}$']
    liste_text = [(30, 30), (0, 30), (45, 0)]
else:
    liste_annotation = ['$Z_{--}$', '$Z_{-+}$', '$Z_{+-}$']
    liste_text = [(30, 30), (45, 0), (0, 30)]

for z in range(0, nb_Z):
    ax[0].annotate('{}'.format(liste_annotation[z]), xy=(eq_st[z][0], eq_st[z][1]), xytext=liste_text[z], size=10,
                   color=liste_color[z], textcoords='offset points', ha='center', va='bottom')
    ax[1].annotate('{}'.format(liste_annotation[z]), xy=(eq_st[z][0], eq_st[z][1]), xytext=liste_text[z], size=10,
                   color=liste_color[z], textcoords='offset points', ha='center', va='bottom')

# Build the matrix of cells for each basin

basins, sims = [], []
cells_P = np.zeros((nb_Z, n_tot, G))
for i in range(0, n_tot):
    cell_E, cell_P = simulate_full(E0, P0, t_simul, theta, K0 * eps, K1 * eps, D1, B * eps, M, S, sigma, G, kon, koff, my_random_choice,
                  update_timeandtau, update_vect)
    sim = odeint(model_relaxation, cell_P[-1], t_simul, args=(theta, D1, K0, K1, S, sigma, M, B, G))
    sims.append(sim[-1, :])
    for z in range(0,nb_Z):
        if np.sum(np.abs(sims[i] - eq_st[z])) < err:
            basins.append(z)
            cells_P[z, i, :] = cell_P[-1, :]

# Plot the Kernel estimation density on each basins

list_color_cmap = ['Blues', 'Greens', 'Reds']
xx, yy = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
positions = np.vstack([xx.ravel(), yy.ravel()])
for z in range(0, nb_Z):
    mask = np.sum(cells_P[z], axis=1) > 0
    values = np.vstack([cells_P[z, mask, 0], cells_P[z, mask, 1]])
    kernel = st.gaussian_kde(values)
    f = np.reshape(kernel(positions).T, xx.shape)
    levels = MaxNLocator(nbins=30).tick_values(0, np.amax(f))
    ax[1].contourf(xx, yy, f, levels=levels[1:], cmap=list_color_cmap[z], alpha=1)
for i in range(0, n_tot):
    ax[1].scatter(cells_P[basins[i], i, 0], cells_P[basins[i], i, 1], c='black', marker='o', s=1,
                  alpha=.2)

# Plot the distribution on the basins

N, bins, patches = ax[2].hist(basins, bins=[0, 1, 2, 3], edgecolor='white', linewidth=1, alpha=.7)
patches[0].set_facecolor('blue')
patches[1].set_facecolor('green')
patches[2].set_facecolor('red')

plt.show()

